export const INITIAL_PILOTS_STATE = [
    {
        title:'CDA :',
        pilotChoice: '',
        pilotChoiceValidity: false,
        piloteDay: '',
        piloteDayValidity : true,
        piloteNight: '',
        piloteNightValidity: true
    },
    {
        title:'pilote :',
        pilotChoice: '',
        pilotChoiceValidity: false,
        piloteDay: '',
        piloteDayValidity : true,
        piloteNight: '',
        piloteNightValidity: true
    }
];
export const INITIAL_DURATION_STATE = [
    {title: 'CAG IFR', durationDay: '', durationDayValidity: true, durationNight: '', durationNightValidity: true},
    {title: 'CAG VFR', durationDay: '', durationDayValidity: true, durationNight: '', durationNightValidity: true},
    {title: 'CAM T', durationDay: '', durationDayValidity: true, durationNight: '', durationNightValidity: true},
    {title: 'CAM V', durationDay: '', durationDayValidity: true, durationNight: '', durationNightValidity: true},
    {title: 'CAM I', durationDay: '', durationDayValidity: true, durationNight: '', durationNightValidity: true}
]
export const INITIAL_FLIGHTFORM_STATE = [
    {value:'', validity: false},
    {value:'', validity: false},
    {value:'', validity: false},
    {value:'', validity: false},
    {value:'', validity: true},
    {value:'', validity: true},
    {value:'', validity: false},
    {value:'', validity: false},
    {value:'', validity: false},
    {value:'', validity: false},
    {value:'', validity: false},
    INITIAL_DURATION_STATE,
    INITIAL_PILOTS_STATE,
];
export const INITIAL_VALIDATEFORM_STATE = [
    ...INITIAL_FLIGHTFORM_STATE,
    {value:'', validity: false, disabled:false},
    {value:'', validity: false, disabled:false},
    {value:'', validity: false, disabled:false},
    {value:'', validity: false},
    {value:'', validity: true, disabled: true},
    {value:'', validity: false}
];