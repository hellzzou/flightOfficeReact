export type control = {
    value: any,
    validity: boolean
    disabled?: boolean
}
export type sectionControl ={ 
    name: string
    display: string
    activity: string
    content: string
}
export type sectionHook = {
    control: sectionControl
    setter: Function
}
export type pilot = {
    title: string,
    pilotChoice: string,
    pilotChoiceValidity: boolean,
    piloteDay: any,
    piloteDayValidity : boolean,
    piloteNight: any,
    piloteNightValidity: boolean

}
export type duration = {
    title: string,
    durationDay: any,
    durationDayValidity : boolean,
    durationNight: any,
    durationNightValidity: boolean

}