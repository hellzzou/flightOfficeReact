import { control, duration, pilot } from "./hooks"
import { pilotHoursView } from "./views"

export type InputProps ={
    inputSize: number,
    backgroundColor: string,
    textColor: string,
    validator: Function,
    type: string,
    min?: number,
    max?: number,
    placeholder?: string,
    control: control,
    setControl: Function,
    durations?: Array<duration>
    setDuration?: Function
    pilots?: Array<pilot>
    setPilot?: Function
}
export type ButtonProps = {
    buttonSize: number,
    buttonThickness: string,
    buttonColor: string,
    buttonMarginX: number,
    buttonContent: string,
    buttonDisplay?: string
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void,
    disabled: true | false
}
export type LabelProps = {
    labelSize: number,
    title: string
}
export type LegendProps = {
    title: string
}
export type SelectProps = {
    selectSize: number, 
    backgroundColor: string,
    textColor: string,
    validator: Function,
    options: string[],
    control: control,
    setControl: Function
    disabled: boolean
    cause?: control
    setCause?: Function
    group?: control
    setGroup?: Function
    client?: control
    setClient?: Function
    manager?: control
    setManager?: Function
}
export type PilotInputProps ={
    inputSize: number,
    backgroundColor: string,
    textColor: string,
    validator: Function,
    type: string,
    min?: number,
    max?: number,
    placeholder?: string,
    index: number,
    pilots: Array<pilot>
    setControl: Function,
    valueToCompare?: any
}
export type DurationInputProps ={
    inputSize: number,
    backgroundColor: string,
    textColor: string,
    validator: Function,
    type: string,
    min?: number,
    max?: number,
    placeholder?: string,
    index: number,
    durations: Array<duration>
    setDuration: Function,
    valueToCompare?: any
}
export type PiloteSelectProps = {
    selectSize: number, 
    backgroundColor: string,
    textColor: string,
    validator: Function,
    options: string[],
    index:number
    pilots: Array<pilot>
    setControl: Function
}
export type ConfirmModalProps = {
    show: boolean
    title: string
    contents: string
    handleClose: (event: React.MouseEvent<HTMLButtonElement>) => void
    confirm: (event: React.MouseEvent<HTMLButtonElement>) => void
}
export type navbarButtonProps = {
    buttonColor: string
    borderColor: string
    buttonMarginX: number
    activity: string
    name: string
    content: string
    onClick: (event: React.MouseEvent<HTMLButtonElement>, name : string) => void,
    disabled: boolean
}
export type ErrorModalProps = {
    show: boolean
    title: string
    contents: Array<string>
    handleClose: (event: React.MouseEvent<HTMLButtonElement>) => void
}
export type pilotCardProps = {
    pilotHours: pilotHoursView
    handleClick: Function
}