import { control, duration, pilot } from "./hooks"
import { activityView, bdvView, pilotView, sumsView } from "./views"

export type timingFieldsetProps = {
    estimatedDateOfDeparture: control,
    setEstimatedDateOfDeparture: Function,
    date: control,
    setDate: Function,
    departureTime: control,
    setDepartureTime: Function,
    estimatedTimeOfDeparture: control,
    setEstimatedTimeOfDeparture: Function,
    dayDuration: control,
    setDayDuration: Function,
    nightDuration: control,
    setNightDuration: Function,
    durations: Array<duration>
    setDuration: Function
    pilots: Array<pilot>
    setPilot: Function
}
export type MissionFieldsetProps = {
    belonging: control,
    setBelonging: Function,
    mission: control,
    setMission: Function,
    flightType: control,
    setFlightType: Function,
    aircraft: control,
    setAircraft: Function,
    crew: control,
    setCrew: Function
    types: Array<string>
    group?: control
    setGroup?: Function
    client?: control
    setClient?: Function
    manager?: control
    setManager?: Function
}
export type DurationFieldsetProps = {
    durations: Array<duration>
    setDuration: Function
    dayValueToCompare: control
    nightValueToCompare: control
}
export type PiloteFieldsetProps = {
    pilotList: Array<string>
    pilots: Array<pilot>
    setPilot: Function
    addPilot: () => void
    dayValueToCompare: control
    nightValueToCompare?: control
}
export type SimpilFieldsetProps = {
    date: control
    setDate: Function
    misssion: control
    setMission: Function
    duration: control
    setDuration: Function
    pilots: Array<pilot>
    setPilot: Function
}
export type AllActivitiesTBodyProps = {
    squadronFlights: Array<activityView>
}
export type MyHoursTFootProps = {
    sums: sumsView
}
export type MyHoursTBodyProps = {
    myHours: Array<pilotView>
    pilot: string
}
export type TheadProps = {
    titles : Array<string>
}
export type TbodyProps = {
    lines: Array<bdvView>
    handleClick: (event: React.MouseEvent<HTMLButtonElement>, dataToValidate: string) => void
    handleDeleteClick: (event: React.MouseEvent<HTMLButtonElement>, id: string, index: number) => void
    primaryButtonContent: string
}