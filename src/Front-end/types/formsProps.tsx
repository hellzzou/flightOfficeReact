import { control, sectionHook } from "./hooks"
import { flightToValidate } from "./models"
import { activityView, pilotView, sumsView } from "./views"

export type AllActivitiesTableProps ={
    squadronFlights: Array<activityView>
    sums:sumsView
}
export type BetweenTwoDatesFormProps = {
    startDate: control
    setStartDate: Function
    endDate: control
    setEndDate: Function
}
export type newEntryProps = {
    display: string
    buttonDisplay: string
    setFlightFormValidity?: Function
    dataToValidate?: flightToValidate
    group?: control
    setGroup?: Function
    client?: control
    setClient?: Function
    manager?: control
    setManager?: Function
    setFlightFormDatasToValidate?: Function
}
export type ModifyTableProps = {
    handleClick: (event: React.MouseEvent<HTMLButtonElement>, dataToValidate: string) => void
    displayValidateTable: string
    startDate: control
    endDate: control
}
export type MyActivitiesTableProps = {
    myHours: Array<pilotView>
    sums: sumsView
    pilot: string
}
export type navbarProps = {
    onClick: (event: React.MouseEvent<HTMLButtonElement>, name : string) => void,
    sectionHooks: Array<sectionHook>
}
export type ValidateFieldsetProps = {
    group: control
    setGroup: Function
    client: control
    setClient: Function
    manager: control
    setManager: Function
    done: control
    setDone: Function
    cause: control
    setCause: Function
    area: control
    setArea: Function
    setValidateFlightValidity?: Function
}
export type ValidateTableProps = {
    handleClick: (event: React.MouseEvent<HTMLButtonElement>, dataToValidate: string) => void
}