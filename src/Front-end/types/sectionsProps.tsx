import { control, sectionHook } from "./hooks"

export type headerProps = {
    onClick: (event: React.MouseEvent<HTMLButtonElement>, name : string) => void,
    sectionHooks: Array<sectionHook>
    update: string
    setUpdate: Function
    userRank: string
    userName: string
    setUser: Function
    setLoginForm: Function
    setFlightOffice: Function
    setLogin: Function
    setPassword: Function
    setLoginError: Function
}
export type loginProps = {
    login: control
    setLogin: Function
    password: control
    setPassword: Function
    onClick: (event: React.MouseEvent<HTMLButtonElement>) => void
    loginError: string
}
export type PliotsHoursProps = {
    display: string
}