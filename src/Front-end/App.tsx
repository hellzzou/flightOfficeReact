import React, { useState } from 'react';
import { INITIAL_USER } from './datas/userDatas';
import { AllActivities } from './Sections/AllActivities';
import {Header} from './Sections/Header';
import { Login } from './Sections/Login';
import { ModifyFlightForm } from './Sections/ModifyFlightForm';
import { MyActivities } from './Sections/MyActivities';
import { NewEntry } from './Sections/newEntry';
import { PilotsHoursTable } from './Sections/PilotsHours';
import { ValidateFlightForm } from './Sections/ValidateflightForm';
import { checkUser } from './Tools/user';

export const App = () => {
  const [login, setLogin] = useState({value:'', validity: false});
  const [password, setPassword] = useState({value:'', validity: false});
  const [user, setUser] = useState(INITIAL_USER);
  const [loginError, setLoginError] = useState('');
  const [loginForm, setLoginForm] = useState('');
  const [flightOffice, setFlightOffice] = useState('d-none');
  const [update, setUpdate] = useState('');
  const [flightSection, setFlightSection] = useState({name: 'flightSection', display: '', activity: 'active', content: 'Nouveau'});
  const [summary, setSummary] = useState({name: 'summary',display: 'd-none', activity: '', content: 'Résumé'});
  const [consultMyhours, setConsultMyHours] = useState({name: 'consultMyhours',display: 'd-none', activity: '', content: 'Mes activités'});
  const [validateFlight, setValidateFlight] = useState({name: 'validateFlight',display: 'd-none', activity: '', content: 'Valider'});
  const [consultHours, setConsultHours] = useState({name: 'consultHours',display: 'd-none', activity: '', content: 'Consulter les heures'});
  const [consultQOG, setConsultQOG] = useState({name: 'consultQOG',display: 'd-none', activity: '', content: 'QOG'});
  const [modifyFlight, setModifyFlight] = useState({name: 'modifyFlight',display: 'd-none', activity: '', content: 'Modifier un vol'});
  const [managedb, setManageDB] = useState({name: 'managedb',display: 'd-none', activity: '', content: 'Base de données'});
  const [stats, setStats] = useState({name: 'stats',display: 'd-none', activity: '', content: 'Statistiques'});
  const sectionHooks = [
    {control: flightSection, setter: setFlightSection},
    {control: summary, setter: setSummary},
    {control: consultMyhours, setter: setConsultMyHours},
    {control: validateFlight, setter: setValidateFlight},
    {control: consultHours, setter: setConsultHours},
    {control: consultQOG, setter: setConsultQOG},
    {control: modifyFlight, setter: setModifyFlight},
    {control: managedb, setter: setManageDB},
    {control: stats, setter: setStats}];

  const handleSection = (event: React.MouseEvent<HTMLButtonElement>, name: string) => {
    sectionHooks.forEach(hook => {
      let display = (hook.control.name === name) ? '' : 'd-none';
      let activity = (hook.control.name === name) ? 'active' : '';
      hook.setter({name: hook.control.name, display: display, activity: activity, content: hook.control.content});
    })
  }
  const handleLogin = (event: React.MouseEvent<HTMLButtonElement>) => {
    (async() => {
      const findUser = await checkUser(login.value, password.value);
      if ( typeof(findUser.userRank) !== 'undefined'){
        setUser(findUser);
        setLoginForm('d-none');
        setFlightOffice('');
        sessionStorage.setItem('token', findUser.token);
      }
      else setLoginError(findUser.error);
    })();
  }
  return (
    <>
      <div className={loginForm}>
        <Login login={login} setLogin={setLogin} password={password} setPassword={setPassword} onClick={handleLogin} loginError={loginError} />
      </div>
      <div className={flightOffice}>
        <Header onClick={handleSection} sectionHooks={sectionHooks} update={update} setUpdate={setUpdate} userRank={user.userRank} userName={user.userName} setUser={setUser} setFlightOffice={setFlightOffice} setLoginForm={setLoginForm} setLogin={setLogin} setPassword={setPassword} setLoginError={setLoginError} />
        <section id="flightSection" className={flightSection.display}><NewEntry /></section>
        <section id="summary" className={summary.display}><PilotsHoursTable display={summary.display} /></section>
        <section id="consultMyhours" className={consultMyhours.display}><MyActivities /></section>
        <section id="validateFlight" className={validateFlight.display}><ValidateFlightForm /></section>
        <section id="consultHours" className={consultHours.display}><AllActivities /></section>
        <section id="consultQOG" className={consultQOG.display}>6</section>
        <section id="modifyFlight" className={modifyFlight.display}><ModifyFlightForm /></section>
        <section id="managedb" className={managedb.display}>8</section>
        <section id="stats" className={stats.display}>9</section>
      </div>
    </>
  )
}
