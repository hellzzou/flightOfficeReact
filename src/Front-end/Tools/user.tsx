import { DB_URL } from "../datas/datas"
import { postFetchRequest } from "./fetch"

export async function createUser(){
    let newUser = {
        login: 'greg',
        password: '10061985',
        rank : 'PM',
        name: 'Piednoël',
        responsability : 'Admin',
        access: 2
    }
    const res = await postFetchRequest(DB_URL+'user/signup', newUser);
    return res;
}
export async function checkUser(login: string, password: string){
    const res = await postFetchRequest(DB_URL+'user/login',{login:login, password: password});
    return res
}