import React, { useState } from 'react';
import { deleteFetchRequest, getFetchRequest } from '../Tools/fetch';
import useAsyncEffect from 'use-async-effect'
import { ValidateTableProps } from '../types/formsProps';
import { DB_URL } from '../datas/datas';
import { Thead } from '../Fieldsets/Thead';
import { ValidateFlightTBody } from '../Fieldsets/ValidateFlightTBody';
import { flightsToBDVView } from '../Tools/transformers';
import { bdvView } from '../types/views';
import { flightToValidate } from '../types/models';

export function ValidateTable(props: ValidateTableProps){
    const titles = ['Date', 'CDA', 'Aéronef', 'Equipage', 'Mission', 'Jour', 'Nuit', 'Total', 'Valider', 'Supprimer'];
    const [BDVFlights, setBDVFlights] = useState([] as Array<bdvView>);

    const handleDeleteClick = (event: React.MouseEvent<HTMLButtonElement>, id: string, index: number) => {
        setBDVFlights([...BDVFlights.slice(0,index),...BDVFlights.slice(index+1, BDVFlights.length)]);
        deleteFetchRequest(DB_URL+'newFlight/deleteWithID', {id:id});
    }

    useAsyncEffect(() => {
        (async () => {
            if (sessionStorage.getItem('token') !== null){
                const flights: Array<flightToValidate> = await getFetchRequest(DB_URL+'newFlight/getAllNewFlights');
                setBDVFlights(flightsToBDVView(flights)) 
            }
        })();
    },[BDVFlights.length]);

    return (
        <div className="table-responsive m-2">
            <table className="table table-sm align-middle table-hover table-secondary rounded table-striped text-center">
                <Thead titles={titles} />     
                <ValidateFlightTBody handleDeleteClick={handleDeleteClick} handleClick={props.handleClick} lines={BDVFlights} primaryButtonContent='valider' />
            </table>
        </div>
    )
}