import React, { useState } from 'react'
import {TimingFieldset} from '../Fieldsets/TimingFieldset'
import {MissionFieldset} from '../Fieldsets/MissionFieldset'
import {DurationFieldset} from '../Fieldsets/DurationFieldset'
import {PiloteFieldset} from '../Fieldsets/PiloteFieldset'
import {ActionButton} from '../BacisComponents/ActionButton'
import { AddPilot, formValidity, resetForm} from '../Tools/formTools'
import {getFetchRequest, postFetchRequest } from '../Tools/fetch';
import useAsyncEffect from 'use-async-effect'
import { ConfirmModal } from '../BacisComponents/ConfirmModal'
import { newEntryProps } from '../types/formsProps'
import { INITIAL_DURATION_STATE, INITIAL_FLIGHTFORM_STATE, INITIAL_PILOTS_STATE } from '../datas/flightFormDatas'
import { DB_URL } from '../datas/datas';
import { saveNewFlight } from '../Tools/saveDatasToDB'


export const FlightForm = (props: newEntryProps) => {
    const [showConfirmModal, setShowConfirmModal] = useState(false);
    const [confirmArray, setConfirmArray] = useState('');
    const [types, setTypes] = useState([] as Array<string>)
    const [pilotList, setPilotList] = useState([] as Array<string>)
    const [estimatedDateOfDeparture, setEstimatedDateOfDeparture] = useState({value:'', validity: false});
    const [estimatedTimeOfDeparture, setEstimatedTimeOfDeparture] = useState({value:'', validity:false});
    const [date, setDate] = useState({value:'', validity:false});
    const [departureTime, setDepartureTime] = useState({value:'', validity:false});
    const [dayDuration, setDayDuration] = useState({value:'', validity:true});
    const [nightDuration, setNightDuration] = useState({value:'', validity:true});
    const [belonging, setBelonging] = useState({value:'', validity:false});
    const [mission, setMission] = useState({value:'', validity:false});
    const [flightType, setFlightType] = useState({value:'', validity:false});
    const [aircraft, setAircraft] = useState({value:'', validity:false});
    const [crew, setCrew] = useState({value:'', validity:false});
    const [durations, setDuration] = useState(INITIAL_DURATION_STATE);
    const [pilots,setPilot] = useState(INITIAL_PILOTS_STATE);
    const hooks = [estimatedDateOfDeparture, estimatedTimeOfDeparture, date,departureTime,dayDuration,nightDuration,belonging,mission,flightType,aircraft,crew];
    const setters = [setEstimatedDateOfDeparture, setEstimatedTimeOfDeparture, setDate, setDepartureTime, setDayDuration, setNightDuration, setBelonging, setMission, setFlightType, setAircraft, setCrew, setDuration, setPilot]
        
    const handleCloseConfirmModal = () => setShowConfirmModal(false);
    
    const handleConfirmModalOpen = () => {
        setConfirmArray("Confirmer l'enregistrement du vol du " + new Date(date.value).toLocaleDateString() + " de l'équipage " + crew.value + " d'une durée de " + (parseFloat(dayDuration.value)+parseFloat(nightDuration.value)).toFixed(1));
        setShowConfirmModal(true);
    }
    
    const handleAddPilot = () => AddPilot(pilots, setPilot)
    
    const createNewFlight = (e: React.MouseEvent<HTMLButtonElement>): void => {
        if (saveNewFlight(pilots, hooks, durations)){
            setShowConfirmModal(false);
            resetForm(setters, INITIAL_FLIGHTFORM_STATE);
        }
    }
    useAsyncEffect(() => {
        (async () => {
            if (sessionStorage.getItem('token') !== null){
                const type = await getFetchRequest(DB_URL+'type/getAllTypes');
                let typeArray= [] as Array<string>;
                type.forEach((type: {_id:string, name: string }) => typeArray.push(type.name));
                setTypes(typeArray);
                const pilots = await postFetchRequest(DB_URL+'pilot/23F', {belonging:'23F'});
                let pilotArray = [] as Array<string>;
                pilots.forEach((pilot: { name: string }) => pilotArray.push(pilot.name));
                setPilotList(pilotArray);
            }
        })();
    },[types.length, pilots.length]);

    return (
        <div>
            <form action="#" className={`row justify-content-center ${props.display}`}>
                <div className="row m-1">
                    <TimingFieldset estimatedDateOfDeparture={estimatedDateOfDeparture} setEstimatedDateOfDeparture={setEstimatedDateOfDeparture} date={date} setDate={setDate} departureTime={departureTime} setDepartureTime={setDepartureTime}  estimatedTimeOfDeparture={estimatedTimeOfDeparture} setEstimatedTimeOfDeparture={setEstimatedTimeOfDeparture}  dayDuration={dayDuration} setDayDuration={setDayDuration}  nightDuration={nightDuration} setNightDuration={setNightDuration} durations={durations} setDuration={setDuration} pilots={pilots} setPilot={setPilot} />
                    <MissionFieldset belonging={belonging}setBelonging={setBelonging} mission={mission}setMission={setMission} flightType={flightType}setFlightType={setFlightType} aircraft={aircraft}setAircraft={setAircraft} crew={crew}setCrew={setCrew} types={types} group={props.group} setGroup={props.setGroup} client={props.client} setClient={props.setClient} manager={props.manager} setManager={props.setManager} />
                </div>
                <div className="row m-1">
                    <DurationFieldset durations={durations} setDuration={setDuration} dayValueToCompare={dayDuration} nightValueToCompare={nightDuration} />
                    <PiloteFieldset pilotList={pilotList} pilots={pilots} setPilot={setPilot} addPilot={handleAddPilot} dayValueToCompare={dayDuration} nightValueToCompare={nightDuration} />
                </div>
                <ActionButton buttonSize={6} buttonThickness="lg" buttonColor="primary" buttonMarginX={3} buttonContent="Enregistrer ce vol" onClick={handleConfirmModalOpen} disabled={!formValidity(hooks, pilots, durations)} buttonDisplay={props.buttonDisplay} />
            </form>
            <ConfirmModal show={showConfirmModal} handleClose={handleCloseConfirmModal} title="Confirmation d'enregistrement" contents={confirmArray} confirm={createNewFlight} />
        </div>
    );
}