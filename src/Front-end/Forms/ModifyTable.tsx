import React, { useState } from 'react';
import { deleteFetchRequest, postFetchRequest } from '../Tools/fetch';
import useAsyncEffect from 'use-async-effect'
import { ModifyTableProps } from '../types/formsProps';
import { DB_URL } from '../datas/datas';
import { Thead } from '../Fieldsets/Thead';
import { ValidateFlightTBody } from '../Fieldsets/ValidateFlightTBody';
import { flightsToBDVView } from '../Tools/transformers';
import { validatedFlight } from '../types/models';
import { bdvView } from '../types/views';

export function ModifyTable(props: ModifyTableProps){
    const titles = ['Date', 'CDA', 'Aéronef', 'Equipage', 'Mission', 'Jour', 'Nuit', 'Total', 'Modifier', 'Supprimer'];
    const [BDVFlights, setBDVFlights] = useState([] as Array<bdvView>);

    const handleDeleteClick = (event: React.MouseEvent<HTMLButtonElement>, id: string, index: number) => {
        setBDVFlights([...BDVFlights.slice(0,index),...BDVFlights.slice(index+1, BDVFlights.length)]);
        deleteFetchRequest(DB_URL+'validatedFlight/deleteOne', {id:id});
    }

    useAsyncEffect(() => {
        (async () => {
            if (sessionStorage.getItem('token') !== null){
                const flights: Array<validatedFlight> = await postFetchRequest(DB_URL+'validatedFlight/byDate', {'startDate':new Date(props.startDate.value), 'endDate':new Date(props.endDate.value)});
                setBDVFlights(flightsToBDVView(flights)) 
            }
        })();
    },[props.startDate, props.endDate]);

    return (
        <div className="table-responsive m-2">
            <table className="table table-sm align-middle table-hover table-secondary rounded table-striped text-center">
                <Thead titles={titles} />     
                <ValidateFlightTBody handleDeleteClick={handleDeleteClick} handleClick={props.handleClick} lines={BDVFlights} primaryButtonContent='modifier' />
            </table>
        </div>
    )
}