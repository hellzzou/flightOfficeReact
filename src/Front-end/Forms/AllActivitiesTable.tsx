import React from 'react';
import { AllActivitiesTBody } from '../Fieldsets/AllActivitiesTBody';
import { AllActivitiesTFoot } from '../Fieldsets/AllActivitiesTFoot';
import { Thead } from '../Fieldsets/Thead';
import { AllActivitiesTableProps } from '../types/formsProps';

export const AllActivitiesTable = (props: AllActivitiesTableProps) => {
    const titles = ["Date", "Avion", "CDA", "Equipage", "Mission", "Effectué", "Cause", "Groupe", "Client", "Gest", "Jour", "Nuit", "Total"];

    return (
        <div className="table-responsive m-2">
            <table className="table table-sm align-middle table-hover table-secondary rounded table-striped text-center">
                <Thead titles={titles} />   
                <AllActivitiesTBody squadronFlights={props.squadronFlights} />
                <AllActivitiesTFoot sums={props.sums}/>
            </table>
        </div>
    )
}