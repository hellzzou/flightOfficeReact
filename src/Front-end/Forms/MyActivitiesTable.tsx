import React from 'react';
import { MyHoursTBody } from '../Fieldsets/MyHoursTBody';
import { MyHoursTFoot } from '../Fieldsets/MyHoursTfoot';
import { Thead } from '../Fieldsets/Thead';
import { MyActivitiesTableProps } from '../types/formsProps';

export const MyActivitiesTable = (props: MyActivitiesTableProps) => {
    const titles = ["Type", "Date", "Avion", "Fonction", "Equipage", "Mission", "Jour", "Nuit", "Total"];

    return (
        <div className="table-responsive m-2">
            <table className="table table-sm align-middle table-secondary rounded text-center">
                <Thead titles={titles} />   
                <MyHoursTBody myHours={props.myHours} pilot={props.pilot} />
                <MyHoursTFoot sums={props.sums}/>
            </table>
        </div>
    )
}