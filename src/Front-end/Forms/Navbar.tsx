import React from 'react';
import { DisplayButton } from '../BacisComponents/DisplayButton';
import { navbarProps } from '../types/formsProps';

export const Navbar = (props: navbarProps) =>
    <div className="nav nav-tabs rounded bg-dark text-light justify-content-center mx-2">
        {props.sectionHooks.map(hook => 
        <DisplayButton 
            buttonColor= "primary"
            borderColor= "dark"
            buttonMarginX= {3}
            name={hook.control.name}
            activity={hook.control.activity} 
            key={hook.control.name} 
            content={hook.control.content} 
            onClick={props.onClick}
            disabled = {false}
            />
        )}
    </div>
