import React, { useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import { determineColor, getMonthString, worthColor } from '../Tools/math';
import { pilotCardProps } from '../types/basicComponentsProps';
import { ActionButton } from './ActionButton';

export const PilotCard = (props: pilotCardProps) =>{
    const [toDoThisMonthColor, setToDoThisMonthColor] = useState('dark');
    const [toDoThisMonthNightColor, setToDoThisMonthNightColor] = useState('dark');
    const [daySinceLastFlightColor, setDaySinceLastFlightColor] = useState('dark');
    const [daySinceLatSimpilColor, setDaySinceLatSimpilColor] = useState('dark');
    const [pilotBG, setPilotBG] = useState('light');

    useEffect(() => {
        setToDoThisMonthColor(determineColor(props.pilotHours.hoursToDoThisMonthTotal, 0, props.pilotHours.norme.hoursToDo / props.pilotHours.norme.duration));
        setToDoThisMonthNightColor(determineColor(props.pilotHours.hoursToDoThisMonthNight, 0, props.pilotHours.norme.nightToDo / props.pilotHours.norme.duration));
        setDaySinceLastFlightColor(determineColor(props.pilotHours.lastFlightdate/1000/60/60/24, 22, 30));
        setDaySinceLatSimpilColor(determineColor(props.pilotHours.lastSimpildate/1000/60/60/24, 22, 30));
        setPilotBG(worthColor([toDoThisMonthColor, toDoThisMonthNightColor, daySinceLastFlightColor, daySinceLatSimpilColor]));
    }, [props.pilotHours.hoursToDoThisMonthTotal, props.pilotHours.norme.hoursToDo, props.pilotHours.norme.duration, props.pilotHours.norme.nightToDo, props.pilotHours.hoursToDoThisMonthNight, props.pilotHours.lastFlightdate, props.pilotHours.lastSimpildate, toDoThisMonthColor, toDoThisMonthNightColor, daySinceLastFlightColor, daySinceLatSimpilColor])
    return (
        <>
        <h5 className="text-center fs-2 fw-bold">{getMonthString(new Date().getMonth())}</h5>
        <Card className="text-center col-md-10 px-0">
            <Card.Header className={`bg-${pilotBG}`}>
                <h5 className={`row m-0 fs-2`}>
                    <div className="col-md-9 text-start">{props.pilotHours.name}</div>
                    <div className="col-md-3 border-start border-dark m-0 text-center">{props.pilotHours.norme.name}</div>
                </h5>
            </Card.Header>
            <Card.Body className="bg-light">
                <div className="row m-2">
                    <Card className="col-md-3 px-0">
                        <Card.Header className="text-center px-2 fs-6 fw-bold">Heures de vol du mois :</Card.Header>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Jour :</div>
                            <div className="col-md-6  m-0 text-center">{(props.pilotHours.lastMonthDay).toFixed(1)}</div>
                        </div>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Nuit :</div>
                            <div className="col-md-6  m-0 text-center">{(props.pilotHours.lastMonthNight).toFixed(1)}</div>
                        </div>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Total :</div>
                            <div className="col-md-6  m-0 text-center">{(props.pilotHours.lastMonthTotal).toFixed(1)}</div>
                        </div>
                    </Card>
                    <Card className="col-md-3 px-0">
                        <Card.Header className="text-center px-2 fs-6 fw-bold">Heures de simpil du mois :</Card.Header>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Total :</div>
                            <div className="col-md-6  m-0 text-center">{(props.pilotHours.lastMonthSimpilTotal).toFixed(1)}</div>
                        </div>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Nuit :</div>
                            <div className="col-md-6  m-0 text-center">{(props.pilotHours.lastMonthSimpilNight).toFixed(1)}</div>
                        </div>
                    </Card>
                    <Card className="col-md-3 px-0">
                        <Card.Header className="text-center px-2 fs-6 fw-bold">6 derniers mois :</Card.Header>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Total :</div>
                            <div className="col-md-6  m-0 text-center">{(props.pilotHours.sixLastMonthTotal).toFixed(1)}</div>
                        </div>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Nuit :</div>
                            <div className="col-md-6  m-0 text-center">{(props.pilotHours.sixLastMonthNight).toFixed(1)}</div>
                        </div>
                    </Card>
                    <Card className="col-md-3 px-0">
                        <Card.Header className="text-center px-2 fs-6 fw-bold">12 derniers mois :</Card.Header>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Total :</div>
                            <div className="col-md-6  m-0 text-center">{(props.pilotHours.twelveLastMonthTotal).toFixed(1)}</div>
                        </div>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Nuit :</div>
                            <div className="col-md-6  m-0 text-center">{(props.pilotHours.twelveLAstMonthNight).toFixed(1)}</div>
                        </div>
                    </Card>
                </div>
                <div className="row m-2">
                    <Card className="col-md-6 px-0">
                        <Card.Header className="text-center px-2 fs-6 fw-bold">Heures à faire ce mois :</Card.Header>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Total :</div>
                            <div className={`col-md-6  m-0 text-center text-${toDoThisMonthColor}`}>{(props.pilotHours.hoursToDoThisMonthTotal).toFixed(1)}</div>
                        </div>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Nuit :</div>
                            <div className={`col-md-6  m-0 text-center text-${toDoThisMonthNightColor}`}>{(props.pilotHours.hoursToDoThisMonthNight).toFixed(1)}</div>
                        </div>
                    </Card>
                    <Card className="col-md-6 px-0">
                        <Card.Header className="text-center px-2 fs-6 fw-bold">Nombre de jours depuis le dernier :</Card.Header>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Vol :</div>
                            <div className={`col-md-6  m-0 text-center text-${daySinceLastFlightColor}`}>{(props.pilotHours.lastFlightdate/1000/60/60/24).toFixed(0)}</div>
                        </div>
                        <div className={`row m-0`}>
                            <div className="col-md-6 text-center">Simpil :</div>
                            <div className={`col-md-6  m-0 text-center text-${daySinceLatSimpilColor}`}>{(props.pilotHours.lastSimpildate/1000/60/60/24).toFixed(0)}</div>
                        </div>
                    </Card>
                </div>
            </Card.Body>
            <Card.Footer className="p-0">
                <ActionButton buttonColor="danger" buttonContent="Retour" buttonMarginX={0} buttonSize={12} buttonThickness="" disabled={false} onClick={() => props.handleClick()} />
            </Card.Footer>
        </Card>
        </>
    )
}