import React from 'react';
import {LegendProps} from '../types/basicComponentsProps'

export const Legend = ({title}: LegendProps) => <legend className="text-center fw-bold">{title}</legend>
