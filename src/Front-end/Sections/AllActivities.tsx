import React, { useState } from 'react';
import useAsyncEffect from 'use-async-effect';
import { DB_URL } from '../datas/datas';
import { INITIAL_MYHOURS, INITIAL_SUMS } from '../datas/pilotDatas';
import { AllActivitiesTable } from '../Forms/AllActivitiesTable';
import { BetweenTwoDatesForm } from '../Forms/BetweenTwoDatesForm';
import { sumActivities } from '../Tools/buildViews';
import { buildSquadronFlights } from '../Tools/buildViews';
import { postFetchRequest } from '../Tools/fetch';
import { getDateNumber, getMonthNumber } from '../Tools/math';
import { activityView } from '../types/views';

export const AllActivities = () => {
    const [startDate, setStartDate] = useState({value:new Date().getFullYear()+'-'+getMonthNumber(new Date().getMonth())+'-01', validity:true});
    const [endDate, setEndDAte] = useState({value:new Date().getFullYear()+'-'+getMonthNumber(new Date().getMonth())+'-'+getDateNumber(new Date().getDate()), validity:true});
    const [squadronFlights, setSquadronFlights] = useState<Array<activityView>>([INITIAL_MYHOURS]);
    const [sums, setSums] = useState(INITIAL_SUMS);

    useAsyncEffect(() => {
        (async() => {
            if (sessionStorage.getItem('token') !== null){
                const allFlights = await postFetchRequest(DB_URL+'validatedFlight/byDate', {'startDate':new Date(startDate.value), 'endDate':new Date(endDate.value)});
                let squadronFlights = buildSquadronFlights(allFlights);
                setSquadronFlights(squadronFlights);
                setSums(sumActivities(squadronFlights));
            }
        })();
    },[startDate, endDate]);

    return (
        <>
            <BetweenTwoDatesForm startDate={startDate} setStartDate={setStartDate} endDate={endDate} setEndDate={setEndDAte} />
            <AllActivitiesTable squadronFlights={squadronFlights} sums={sums} />
        </>
    )
}