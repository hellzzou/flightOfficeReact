import React, { useState } from 'react';
import useAsyncEffect from 'use-async-effect';
import { PilotCard } from '../BacisComponents/PilotCard';
import { PilotMiniCard } from '../BacisComponents/PilotMiniCard';
import { INITIAL_PILOTHOURS } from '../datas/pilotDatas';
import { buildPilotsHours } from '../Tools/buildViews';
import { sliceArray } from '../Tools/math';
import { PliotsHoursProps } from '../types/sectionsProps';
import { pilotHoursView } from '../types/views';

export const PilotsHoursTable = (props: PliotsHoursProps) => {
    const [lines, setLines] = useState([INITIAL_PILOTHOURS]);
    const [pilotHours, setPilotHours] = useState(INITIAL_PILOTHOURS);
    const [displayPersonnalCard, setDisplayPersonalCard] = useState('d-none');
    const [displayMiniCards, setDisplayMiniCards] = useState('');

    const handleClick = (pilotHours: pilotHoursView) => {
        setPilotHours(pilotHours);
        setDisplayPersonalCard('');
        setDisplayMiniCards('d-none');
    }
    const handleReturnClick = () => {
        setDisplayPersonalCard('d-none');
        setDisplayMiniCards('');
    }

    useAsyncEffect(() =>{
        (async() => {
            if (sessionStorage.getItem('token') !== null){
                const pilotHours = await buildPilotsHours();
                setLines(pilotHours);
            }
        })();
    }, [props.display])
    return (
        <>
            <div className={displayMiniCards}>
                {sliceArray(lines, 6).map(line => 
                    <div key={line[0].name} className="row m-2">
                        {line.map(card => <PilotMiniCard key={card.name} pilotHours={card} handleClick={handleClick} />)}
                    </div>
                )}
            </div>
            <div className={`${displayPersonnalCard} row m-2 justify-content-center`}>
                <PilotCard pilotHours={pilotHours} handleClick={handleReturnClick} />
            </div>
        </>
    )
}

