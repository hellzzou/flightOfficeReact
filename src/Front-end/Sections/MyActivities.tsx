import React, { useState } from 'react';
import useAsyncEffect from 'use-async-effect';
import { INITAL_PILOT_HOURS, INITIAL_SUMS } from '../datas/pilotDatas';
import { BetweenTwoDatesForm } from '../Forms/BetweenTwoDatesForm';
import { MyActivitiesTable } from '../Forms/MyActivitiesTable';
import { buildMyHours, sumActivities } from '../Tools/buildViews';
import { getDateNumber, getMonthNumber } from '../Tools/math';
import { pilotView } from '../types/views';

export const MyActivities = () => {
    const [startDate, setStartDate] = useState({value:new Date().getFullYear()+'-'+getMonthNumber(new Date().getMonth())+'-01', validity:true});
    const [endDate, setEndDAte] = useState({value:new Date().getFullYear()+'-'+getMonthNumber(new Date().getMonth())+'-'+getDateNumber(new Date().getDate()), validity:true});
    const [myHours, setMyHours] = useState<Array<pilotView>>([INITAL_PILOT_HOURS]);
    const [sums, setSums] = useState(INITIAL_SUMS);
    const [pilot, setPilot] = useState('Villaume');

    useAsyncEffect(() => {
        (async () => {
            if (sessionStorage.getItem('token') !== null){
                const allPilotHours = await buildMyHours(pilot, new Date(startDate.value), new Date(endDate.value));
                setMyHours(allPilotHours);
                setSums(sumActivities(allPilotHours));
            }
        })();
    },[startDate, endDate]);
    return (
        <>
            <BetweenTwoDatesForm startDate={startDate} setStartDate={setStartDate} endDate={endDate} setEndDate={setEndDAte} />
            <MyActivitiesTable myHours={myHours} sums={sums} pilot={pilot} />
        </>
    )
}