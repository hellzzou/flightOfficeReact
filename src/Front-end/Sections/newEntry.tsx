import React, { useState } from 'react';
import { DisplayButton } from '../BacisComponents/DisplayButton';
import { FlightForm } from '../Forms/FlightForm';
import { SimpilForm } from '../Forms/SimpilForm';

export const NewEntry = () => {
    const [flightFormDisplay, setFlightFormDisplay] = useState({display:'', activity:'active'});
    const [simpilFormDisplay, setSimpilFormDisplay] = useState({display: "d-none", activity: ''});
    const handleClick = (e: any, name?: string) => {
        if (name === 'flight'){ 
            setFlightFormDisplay({display: '',activity: 'active'}); 
            setSimpilFormDisplay({display: 'd-none', activity: ''});
        }
        else {
            setFlightFormDisplay({display: 'd-none', activity: ''});
            setSimpilFormDisplay({display: '',activity: 'active'});
        }
    }
    return (
        <div>
            <div className="row justify-content-center my-1">
                <div className="col-md-2">
                    <DisplayButton borderColor="primary"  buttonColor="primary" buttonMarginX={3} name="flight" activity={flightFormDisplay.activity} content="Nouveau vol" onClick={handleClick} disabled={false} />
                </div>
                <div className="col-md-4"></div>
                <div className="col-md-2">
                    <DisplayButton borderColor="primary" buttonColor="primary" buttonMarginX={3} name="simpil" activity={simpilFormDisplay.activity} content="Nouveau simpil" onClick={handleClick} disabled={false} />
                </div>
            </div>
            <div>
                <FlightForm display={flightFormDisplay.display} buttonDisplay=''/>
                <SimpilForm display={simpilFormDisplay.display} buttonDisplay=''/>
            </div>
        </div>
    )
}