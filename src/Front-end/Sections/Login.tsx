import React from 'react';
import { ActionButton } from '../BacisComponents/ActionButton';
import { Input } from '../BacisComponents/Input';
import { Label } from '../BacisComponents/Label';
import { Legend } from '../BacisComponents/Legend';
import { formValidity } from '../Tools/formTools';
import { loginProps } from '../types/sectionsProps';
import { textIsNotNull } from '../Tools/validators';
import loginbg from '../images/loginbg1.jpg';

export const Login = (props: loginProps) => {
    const divStyle={
        backgroundImage: `url(${loginbg})`,
        backgroundSize: 'cover',
        height: '100vh'
    }
    return (
        <>
            <form action="#" className="row justify-content-center align-items-center bg-image" style={divStyle}>
                <div className="col-md-5">
                    <fieldset className="border border-dark text-dark rounded py-4 bg-light">
                        <Legend title="Connexion" />
                        <div className="form-group row m-3 mt-5">
                            <Label labelSize={4} title="Identifiant :" />
                            <Input inputSize={8} backgroundColor="dark" textColor="light" validator={textIsNotNull} type="text" control={props.login} setControl={props.setLogin} placeholder="Entrez votre identifiant..." />
                        </div>
                        <div className="form-group row m-3">
                            <Label labelSize={4} title="Mot de passe :" />
                            <Input inputSize={8} backgroundColor="dark" textColor="light" validator={textIsNotNull} type="password" control={props.password} setControl={props.setPassword} placeholder="Entrez votre mot de passe..." />
                        </div>
                        <div className="row m-1 justify-content-center">
                            <ActionButton buttonColor="primary" buttonContent="LOGIN" buttonSize={8} buttonThickness="md" buttonMarginX={0} disabled={!formValidity([props.login, props.password])} onClick={props.onClick} />
                        </div>
                        <div className="justify-content-center row mt-3 text-danger fw-bold"><span className="col-md-4">{props.loginError}</span></div>
                    </fieldset>
                </div>
            </form>
        </>
    )
}