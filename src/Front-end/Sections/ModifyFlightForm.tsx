import React, { useState } from 'react';
import { ActionButton } from '../BacisComponents/ActionButton';
import { ValidateFieldset } from '../Forms/ValidateFieldset';
import { ConfirmModal } from '../BacisComponents/ConfirmModal';
import { TimingFieldset } from '../Fieldsets/TimingFieldset';
import { MissionFieldset } from '../Fieldsets/MissionFieldset';
import { PiloteFieldset } from '../Fieldsets/PiloteFieldset';
import { DurationFieldset } from '../Fieldsets/DurationFieldset';
import { AddPilot, formValidity, fullfillFlightFormWithValidatedFlight, resetForm } from '../Tools/formTools';
import { INITIAL_DURATION_STATE, INITIAL_PILOTS_STATE, INITIAL_VALIDATEFORM_STATE } from '../datas/flightFormDatas';
import useAsyncEffect from 'use-async-effect';
import { deleteFetchRequest, getFetchRequest, postFetchRequest } from '../Tools/fetch';
import { DB_URL } from '../datas/datas';
import { saveValidatedFlight } from '../Tools/saveDatasToDB';
import { BetweenTwoDatesForm } from '../Forms/BetweenTwoDatesForm';
import { getDateNumber, getMonthNumber } from '../Tools/math';
import { ModifyTable } from '../Forms/ModifyTable';

export const ModifyFlightForm = () => {
    const [startDate, setStartDate] = useState({value:new Date().getFullYear()+'-'+getMonthNumber(new Date().getMonth())+'-01', validity:true});
    const [endDate, setEndDAte] = useState({value:new Date().getFullYear()+'-'+getMonthNumber(new Date().getMonth())+'-'+getDateNumber(new Date().getDate()), validity:true});
    const [displayModifyForm, setDisplayModifyForm] = useState('d-none');
    const [displayModifyTable, setDisplayModifyTable] = useState('');
    const [dataToValidate, setDataToValidate] = useState('');
    const [types, setTypes] = useState([] as Array<string>);
    const [pilotList, setPilotList] = useState([] as Array<string>);
    const [estimatedDateOfDeparture, setEstimatedDateOfDeparture] = useState({value:'', validity: false});
    const [estimatedTimeOfDeparture, setEstimatedTimeOfDeparture] = useState({value:'', validity:false});
    const [date, setDate] = useState({value:'', validity:false});
    const [departureTime, setDepartureTime] = useState({value:'', validity:false});
    const [dayDuration, setDayDuration] = useState({value:'', validity:true});
    const [nightDuration, setNightDuration] = useState({value:'', validity:true});
    const [belonging, setBelonging] = useState({value:'', validity:false});
    const [mission, setMission] = useState({value:'', validity:false});
    const [flightType, setFlightType] = useState({value:'', validity:false});
    const [aircraft, setAircraft] = useState({value:'', validity:false});
    const [crew, setCrew] = useState({value:'', validity:false});
    const [durations, setDuration] = useState(INITIAL_DURATION_STATE);
    const [pilots,setPilot] = useState(INITIAL_PILOTS_STATE);
    const hooks = [estimatedDateOfDeparture, estimatedTimeOfDeparture, date,departureTime,dayDuration,nightDuration,belonging,mission,flightType,aircraft,crew];
    const [group, setGroup] = useState({value:'', validity: false, disabled:false});
    const [client, setClient] = useState({value:'', validity: false, disabled: false});
    const [manager, setManager] = useState({value:'', validity: false, disabled: false});
    const [done, setDone] = useState({value:'', validity: false});
    const [cause, setCause] = useState({value:'', validity: true, disabled: true});
    const [area, setArea] = useState({value:'', validity: false});
    const setters = [setEstimatedDateOfDeparture, setEstimatedTimeOfDeparture, setDate, setDepartureTime, setDayDuration, setNightDuration, setBelonging, setMission, setFlightType, setAircraft, setCrew, setDuration, setPilot, setGroup, setClient, setManager, setDone, setCause, setArea];
    const additionals = [group, client, manager, done, cause, area];
    const [confirmArray, setConfirmArray]= useState('');
    const [showConfirmModal, setShowConfirmModal] = useState(false);

    const handleAddPilot = () => AddPilot(pilots, setPilot)
    const handleTableClick = (event: React.MouseEvent<HTMLButtonElement>, id: string) =>  {
        setDataToValidate(id);
        fullfillFlightFormWithValidatedFlight(id, setters);
        setDisplayModifyForm('');
        setDisplayModifyTable('d-none');
    }
    const handleReturnClick = () => {
        setDisplayModifyForm('d-none');
        setDisplayModifyTable('');
        resetForm(setters, INITIAL_VALIDATEFORM_STATE);
    }
    const handleConfirmModalOpen = () => {
        setConfirmArray("Etes-vous sûr de vouloir modifier ce vol ?");
        setShowConfirmModal(true);
    }
    const handleCloseConfirmModal = () => setShowConfirmModal(false);
    const validateflight = (e: React.MouseEvent<HTMLButtonElement>): void => {
        deleteFetchRequest(DB_URL+'validatedFlight/deleteOne',{id:dataToValidate});
        if ( saveValidatedFlight(hooks, durations, pilots, additionals, dataToValidate)){
            setShowConfirmModal(false);
            setDisplayModifyForm('d-none');
            setDisplayModifyTable('');
            resetForm(setters, INITIAL_VALIDATEFORM_STATE);
        }
    }

    useAsyncEffect(() => {
        (async () => {
            const type = await getFetchRequest(DB_URL+'type/getAllTypes');
            let typeArray= [] as Array<string>;
            type.forEach((type: {_id:string, name: string }) => typeArray.push(type.name));
            setTypes(typeArray);
            const pilots = await postFetchRequest(DB_URL+'pilot/23F', {belonging:'23F'});
            let pilotArray = [] as Array<string>;
            pilots.forEach((pilot: { name: string }) => pilotArray.push(pilot.name));
            setPilotList(pilotArray);
        })();
    },[types.length, pilots.length]);

    return (
        <>
            <div className={displayModifyTable}>
                <BetweenTwoDatesForm startDate={startDate} setStartDate={setStartDate} endDate={endDate} setEndDate={setEndDAte} />
                <ModifyTable handleClick={handleTableClick} displayValidateTable={displayModifyTable} startDate={startDate} endDate={endDate} />
            </div>
            <div className={displayModifyForm}>
                <form action="#" className="row justify-content-center">
                    <div className="row m-1">
                        <TimingFieldset estimatedDateOfDeparture={estimatedDateOfDeparture} setEstimatedDateOfDeparture={setEstimatedDateOfDeparture} date={date} setDate={setDate} departureTime={departureTime} setDepartureTime={setDepartureTime}  estimatedTimeOfDeparture={estimatedTimeOfDeparture} setEstimatedTimeOfDeparture={setEstimatedTimeOfDeparture}  dayDuration={dayDuration} setDayDuration={setDayDuration}  nightDuration={nightDuration} setNightDuration={setNightDuration} durations={durations} setDuration={setDuration} pilots={pilots} setPilot={setPilot} />
                        <MissionFieldset belonging={belonging}setBelonging={setBelonging} mission={mission}setMission={setMission} flightType={flightType}setFlightType={setFlightType} aircraft={aircraft}setAircraft={setAircraft} crew={crew}setCrew={setCrew} types={types} group={group} setGroup={setGroup} client={client} setClient={setClient} manager={manager} setManager={setManager} />
                    </div>
                    <div className="row m-1">
                        <DurationFieldset durations={durations} setDuration={setDuration} dayValueToCompare={dayDuration} nightValueToCompare={nightDuration} />
                        <PiloteFieldset pilotList={pilotList} pilots={pilots} setPilot={setPilot} addPilot={handleAddPilot} dayValueToCompare={dayDuration} nightValueToCompare={nightDuration} />
                    </div>
                </form>
                <ValidateFieldset group={group} setGroup={setGroup} client={client} setClient={setClient} manager={manager} setManager={setManager} done={done} setDone={setDone} cause={cause} setCause={setCause} area={area} setArea={setArea} />
                <div className="row justify-content-center mb-2">
                    <ActionButton onClick={handleReturnClick} buttonSize={4} buttonColor="danger" buttonContent="Retour" buttonMarginX={1} buttonThickness="md" disabled={false}/>
                    <ActionButton onClick={handleConfirmModalOpen} buttonSize={4} buttonColor="primary" buttonContent="Valider ce vol" buttonMarginX={1} buttonThickness="md" disabled={!formValidity(hooks, pilots, durations, additionals)}/>
                </div>
                <ConfirmModal show={showConfirmModal} title="Confirmation de validation" contents={confirmArray} handleClose={handleCloseConfirmModal} confirm={validateflight} />
            </div>
        </>
    )
}