import React, { useState } from 'react';
import { DisplayButton } from '../BacisComponents/DisplayButton';
import { Navbar } from '../Forms/Navbar'
import { headerProps } from '../types/sectionsProps';
import image from '../images/whiteAircraft.png';
import useAsyncEffect from 'use-async-effect';
import { getLastEntry } from '../Tools/buildViews';
import { determineColor } from '../Tools/math';
import { INITIAL_USER } from '../datas/userDatas';

export const Header = (props: headerProps) => {
    const [updateColor, setUpdateColor] = useState('success');
    const handleClick = () => {
        props.setUser(INITIAL_USER);
        props.setLoginForm('');
        props.setFlightOffice('d-none');
        props.setLogin({value:'', validity:false});
        props.setPassword({value:'', validity:false});
        props.setLoginError('');
    }
    useAsyncEffect(() => {
        (async()=> {
            if (sessionStorage.getItem('token') !== null){
                const lastEntry = await getLastEntry();
                props.setUpdate(new Date(lastEntry).toLocaleDateString());
                setUpdateColor(determineColor((Date.now() - Date.parse('07/01/2021'))/1000/60/60/24,7,14))
            }
        })();
    })
    return (
        <header>
            <div className="bg-dark rounded mx-2 my-1">
                <div className="align-items-center row mx-3 p-2">
                    <div className="col-md-2 text-center text-light align-self-center"><img src={image} alt="lol" /><h5 className="d-inline mx-3">Bureau des vols</h5></div>
                    <div className="col-md-2"></div>
                    <h5 className="col-md-3 text-center text-light align-self-center pt-2">Dernière mise à jour : <span className={`text-${updateColor}`}>{props.update}</span></h5>
                    <div className="col-md-2"></div>
                    <div className="col-md-3 text-center text-light align-self-center">
                        <h5 className="d-inline px-3">{`${props.userRank} ${props.userName}`}</h5>
                        <DisplayButton buttonColor="warning" borderColor="warning" buttonMarginX={0} activity="" name="disconnect" content="Déconnexion" onClick={handleClick} disabled={false} />
                    </div>
                </div>
            </div>
            <Navbar onClick={props.onClick} sectionHooks={props.sectionHooks} />
        </header>
    )
}