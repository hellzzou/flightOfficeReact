const jwt = require('jsonwebtoken');
const user = require('../models/user');

module.exports = (req, res, next ) => {
    const routeAccess = new Map([
        ['/api/area/deleteAllAreas', 2],
        ['/api/area/createAnArea', 2],
        ['/api/area/getAllAreas', 0],
        ['/api/group/findManager', 1],
        ['/api/group/findClient', 1],
        ['/api/group/findUnderGroup', 1],
        ['/api/group/findGroup', 1],
        ['/api/group/save', 2],
        ['/api/group/getAllGroups', 1],
        ['/api/group/deleteAllGroups', 2],
        ['/api/newFlight/save', 0],
        ['/api/newFlight/find', 1],
        ['/api/newFlight/getAllNewFlights', -1],
        ['/api/newFlight/deleteWithID', 1],
        ['/api/norm/deleteAllNorms', 2],
        ['/api/norm/find', -1],
        ['/api/norm/getAllNorms', -1],
        ['/api/norm/save', 2],
        ['/api/pilot/getAllPilots', 2],
        ['/api/pilot/deleteAllPilots', 2],
        ['/api/pilot/23F', -1],
        ['/api/pilot/save', 2],
        ['/api/pilot/getOne', 0],
        ['/api/simpil/save', 0],
        ['/api/simpil/byDate', -1],
        ['/api/type/getAllTypes', -1],
        ['/api/type/deleteAllTypes', 2],
        ['/api/type/createAType', 2],
        ['/api/user/signup', 2],
        ['/api/user/login', -1],
        ['/api/user/getAllUsers', 2],
        ['/api/user/getOne', 0],
        ['/api/user/deleteAUser', 2],
        ['/api/user/getAdmins', -1],
        ['/api/validatedFlight/save', 1],
        ['/api/validatedFlight/find', 2],
        ['/api/validatedFlight/deleteOne', 2],
        ['/api/validatedFlight/byDate', -1],
    ]);
    try{
        const access = routeAccess.get(req.originalUrl);
        if ( access === -1) next();
        else {
            try{
                const token = req.headers.authorization.split(' ')[1];
                const decodedToken = jwt.verify(token, 'FLIGHT_OFFICE_TOKEN_ACCESS_SECRET');
                const userID = decodedToken.userID;
                user.findOne({_id:userID})
                    .then(user => {
                        const access = routeAccess.get(req.originalUrl);
                        if ( user.access < access) res.status.json({error: "Vous n'êtes pas autorisé à faire cette requète"});
                        else next();
                    })
                    .catch(res.status(401).json({error:'Aucun utilisateur avec ce token'}))
            }catch(error){
                res.status(401).json({error:"Vous n'êtes pas authentifié"})
            }
        } 
    }catch(error){
        res.status(401).json({error:"Cette route n'a pas d'accès"})
    }
}