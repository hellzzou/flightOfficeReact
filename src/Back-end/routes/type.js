const express = require('express');
const router = express.Router();
const TypeCtrl = require('../controllers/type');
const auth = require('../authentification/auth');

router.get('/getAllTypes', auth, TypeCtrl.getAllTypes);
router.delete('/deleteAllTypes', auth, TypeCtrl.deleteAllTypes);
router.post('/createAType', auth, TypeCtrl.crateAtype);

module.exports = router;