const express = require('express');
const router = express.Router();
const AreaCtrl = require('../controllers/area');
const auth = require('../authentification/auth');

router.get('/getAllAreas', auth, AreaCtrl.findAllAreas);
router.delete('/deleteAllAreas', auth, AreaCtrl.deleteAllAreas);
router.post('/createAnArea', auth, AreaCtrl.createAnArea);

module.exports = router;