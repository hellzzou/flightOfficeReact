const express = require('express');
const router = express.Router();
const UserCtrl = require('../controllers/user');
const auth = require('../authentification/auth');

router.post('/signup', auth, UserCtrl.signup);
router.post('/login', UserCtrl.login);
router.get('/getAllUsers', auth, UserCtrl.getAllUsers);
router.get('/getOne', UserCtrl.getAUser);
router.delete('/deleteAUser', auth, UserCtrl.deleteOneUser);
router.post('/getAdmins', auth, UserCtrl.getUsersByFunction);


module.exports = router;