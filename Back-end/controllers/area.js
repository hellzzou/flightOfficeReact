const Area = require('../models/area');

exports.findAllAreas = (req, res, next) => {
	Area.find({})
		.then(Area => {return res.status(200).json(Area)})
		.catch(error => {return res.status(400).json({error})});
};
exports.deleteAllAreas = (req, res, next) => {
	Area.remove({})
		.then(() => {return res.status(200).json('deleted')})
		.catch(error => {return res.status(400).json({error})});
};
exports.createAnArea = (req, res, next) => {
	let area = new Area(req.body.area);
	area.save(function(err){
		if (err) return res.status(400).json("Erreur de connexion !");
		else return res.status(200).json("success");
	})
};