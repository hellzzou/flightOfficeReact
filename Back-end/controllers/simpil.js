const Simpil = require('../models/simpil');

exports.createSimpil = (req, res) => {
	let simpil = new Simpil(req.body);
	simpil.save(function(err){
		if (err) return res.status(400).json("Erreur de connexion !");
		else return res.status(200).json("success");
	})
};
exports.getSimpilsBetweenTwodates = (req, res) => {
	let query = {effectiveDeparture:{$gt:req.body.startDate, $lt:req.body.endDate}};
	Simpil.find(query)
		.then(Simpil => {return res.status(200).json(Simpil)})
		.catch(error => {return res.status(400).json({error})});
};