const NewFlight = require('../models/newflight');

exports.createNewFlight = (req, res) => {
	let flight = new NewFlight(req.body);
	flight.save(function(err){
		if (err) return res.status(400).json("Erreur de connexion !");
		else return res.status(200).json("success");
	})
};
exports.findNewFlightWithID = (req, res) => {
	let query = {_id:req.body.id};
	NewFlight.findOne(query)
		.then(NewFlight => {return res.status(200).json(NewFlight)})
		.catch(error => {return res.status(400).json({error:"Vol non sauvegardé"})});
};
exports.getAllNewFlights =(req, res, next) => {
	NewFlight.find({})
		.then(NewFlight => {return res.status(200).json(NewFlight)})
		.catch(error => {return res.status(400).json({error})});
};
exports.deleteNewFlightWithID =(req,res,next) =>{
	NewFlight.deleteOne({_id: req.body.id})
		.then(() => {return res.status(200).json("success")})
		.catch(error => {return res.status(400).json({error})});
};