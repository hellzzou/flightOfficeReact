const ValidatedFlight = require('../models/validatedflight');

exports.createAValidatedFlight = (req, res) => {
	let flight = new ValidatedFlight(req.body.flight);
	flight.save(function(err){
		if (err) return res.status(400).json("Erreur de connexion !");
		else return res.status(200).json("success");
	})
};
exports.findOneValidatedFlight = (req, res) => {
	let query = {_id:req.body.id};
	ValidatedFlight.findOne(query)
		.then(NewFlight => {return res.status(200).json(NewFlight)})
		.catch(error => {return res.status(400).json({error})});
};
exports.deleteOneValidatedFlight = (req,res,next) =>{
	ValidatedFlight.deleteOne({_id: req.body.id})
		.then(() => {return res.status(200).json('success')})
		.catch(error => {return res.status(400).json({error})});
};
exports.findValidateFlightsBetweenThisDates = (req, res) => {
	let query = {effectiveDeparture:{$gt:req.body.startDate, $lt:req.body.endDate}};
	ValidatedFlight.find(query)
		.then(ValidatedFlight => {return res.status(200).json(ValidatedFlight)})
		.catch(error => {return res.status(400).json({error})});
};