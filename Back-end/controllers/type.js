const Type = require('../models/type');

exports.getAllTypes = (req, res, next) => {
	Type.find({})
		.then(Type => {return res.status(200).json(Type)})
		.catch(error => {return res.status(400).json({error})});
};
exports.deleteAllTypes = (req, res) => {
	Type.remove({})
		.then(() => {return res.status(200).json('deleted')})
		.catch(error => {return res.status(400).json({error})});
};
exports.crateAtype = (req, res) => {
	let type = new Type(req.body.type);
	type.save(function(err){
		if (err) return res.status(400).json("Erreur de connexion !");
		else return res.status(200).json("success");
	})
};