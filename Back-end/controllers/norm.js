const Norme = require('../models/norme');

exports.deleteAllNorms = (req, res) => {
	Norme.remove({})
		.then(() => {return res.status(200).json('deleted')})
		.catch(error => {return res.status(400).json({error})});
};
exports.findNormWithName = (req, res) => {
	let query = {name:req.body.name};
	Norme.find(query)
		.then(Norme => {return res.status(200).json(Norme)})
		.catch(error => {return res.status(400).json({error})});
};
exports.getAllNorms = (req, res, next) => {
	Norme.find({})
		.then(Norme => {return res.status(200).json(Norme)})
		.catch(error => {return res.status(400).json({error})});
};
exports.createNorm =(req, res) => {
	let norm = new Norme(req.body.norm);
	norm.save(function(err){
		if (err) return res.status(400).json("Erreur de connexion !");
		else return res.status(200).json("success");
	})
};